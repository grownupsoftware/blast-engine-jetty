![Alt text](http://www.gusl.co/blast/resources/images/b-circle-trans-100.png) *on* ![Alt test](http://www.eclipse.org/jetty/images/jetty-logo-80x22.png)

Built using jetty version 9.4.2.v20170220

# Gradle
```groovy
repositories { maven { url "http://dl.bintray.com/gusl/blast" } }

compile 'co.gusl:blast-engine-jetty:1.0.0'
```


# Embedded


Simply add a new route to the router as follows.

```java
        ServletHolder holder = new ServletHolder();
        holder.setServlet(new JettyBlastServlet(server));
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(holder, BlastConstants.BLAST_ROUTE_PATH);
```


# Stand alone

To run the Standalone version of blast use ..

```java
     BlastServer blast = Blast.blast(new JettyEngine())
```
This will create a Jetty HTTP Server and configure using the properties from Blast Properties.
As the `JettyEngine` is a `Controllable` Blast will start and stop the engine.

## Gradle Runner

`./gradlew jetty:jetty-engine:run`
