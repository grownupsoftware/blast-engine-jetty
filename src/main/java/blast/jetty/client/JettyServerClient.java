/*
 * Grownup Software Limited.
 */
package blast.jetty.client;

import blast.client.AbstractBlastServerClient;
import blast.client.ClosingReason;
import blast.server.BlastServer;
import java.io.IOException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import blast.client.WebSocketRequestDetails;

/**
 *
 * @author dhudson - Mar 28, 2017 - 2:26:58 PM
 */
@WebSocket
public class JettyServerClient extends AbstractBlastServerClient {

    private Session session;
    private String clientID;
    private final BlastServer blastServer;

    public JettyServerClient(BlastServer blastServer, WebSocketRequestDetails details) {
        super(blastServer, details);
        this.blastServer = blastServer;
    }

    @Override
    public void close(ClosingReason reason) {
        if (session != null) {
            session.close();
        }

        session = null;
    }

    @Override
    public String getClientID() {
        return clientID;
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        session.getRemote().sendString(new String(bytes));
    }

    @Override
    public void queueMessage(byte[] bytes) {
        postQueuedMessageEvent(bytes);
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;
        clientID = createShortClientID();
        // Let Blast deal with it
        postClientConnectingEvent();
    }

    @OnWebSocketMessage
    public void onText(String message) {
        postClientInboundMessageEvent(message);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        postClientClosed(ClosingReason.CLOSED_BY_CLIENT);
    }
}
