/*
 * Grownup Software Limited.
 */
package blast.jetty.server;

import blast.Blast;
import blast.exception.BlastException;
import blast.jetty.engine.JettyEngine;
import blast.module.echo.EchoModule;
import blast.module.ping.PingModule;
import blast.server.BlastServer;

/**
 *
 * @author dhudson - Mar 28, 2017 - 11:50:46 AM
 */
public class BlastJettyServer {

    public static void main(String[] args) {
        try {

            BlastServer blast = Blast.blast(new JettyEngine(), new PingModule(), new EchoModule());

            blast.startup();
        } catch (BlastException ex) {
            Blast.logger.warn("Can't start Blast!", ex);
        }
    }
}
