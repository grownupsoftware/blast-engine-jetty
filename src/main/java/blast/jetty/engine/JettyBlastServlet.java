/*
 * Grownup Software Limited.
 */
package blast.jetty.engine;

import static blast.BlastConstants.BLAST_ROUTE_PATH;
import blast.server.BlastServer;
import javax.servlet.annotation.WebServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

/**
 *
 * @author dhudson - Mar 28, 2017 - 2:28:18 PM
 */
@WebServlet(name = "Blast!", urlPatterns = {BLAST_ROUTE_PATH})
public class JettyBlastServlet extends WebSocketServlet {
    
    private final BlastServer server;

    public JettyBlastServlet(BlastServer blastServer) {
        server = blastServer;
    }

    @Override
    public void configure(WebSocketServletFactory wssf) {
        wssf.setCreator(new BlastWebSocketCreator(server));
    }

}
